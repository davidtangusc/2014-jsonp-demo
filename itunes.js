var songTemplateFunction = Handlebars.compile($('#song-template').html());

var search = function(searchTerm) {
  searchTerm = encodeURIComponent(searchTerm);
  var url = 'https://itunes.apple.com/search?term='+searchTerm+'&callback=?';
  jQuery.getJSON(url, function(response) {
    console.log(response);

    var html = '';

    for (var i = 0; i < response.results.length; i++) {
      html += songTemplateFunction(response.results[i]);
    }

    $('#results').html(html);
  });
};

$('form').on('submit', function(e) {
  e.preventDefault();

  var searchTerm = $('#search-term').val();

  console.log(searchTerm);

  $('#results').html('Loading...');
  search(searchTerm);
});